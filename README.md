# Low and Slow

Demo environment and commands to illustrate a low and slow attack works as part of the discussion about the Mr. Robot pilot episode where Evil Corp experiences a RUDY attack (R U Dead Yet?, a denial-of-service tool that slowly incapacitates a server through constant attacks)

## Description

A low and slow attack is a type of DoS or DDoS attack that relies on a small stream of very slow traffic targeting application or server resources. Unlike more traditional brute-force attacks, low and slow attacks require very little bandwidth and can be hard to mitigate, as they generate traffic that is very difficult to distinguish from normal traffic. While large-scale DDoS attacks are likely to be noticed quickly, low and slow attacks can go on undetected for long periods of time, all while denying or slowing service to real users.

Because they do not require a lot of resources to pull off, low and slow attacks can be successfully launched using a single computer, as opposed to more distributed attacks that may require a botnet. Two of the most popular tools for launching a low and slow attack are called Slowloris and R.U.D.Y.

## Thread-based Server Architectures

The Apache HTTP Server, also known as Apache HTTPd, is a widely used open source web server that is extremely customizable. However, Apache’s original model of one process or thread per connection does not scale well for thousands of concurrent requests, which has paved the way for other types of web servers to gain popularity. One such web server is NGINX, which was designed to address the C10k problem: How can web servers handle 10,000 clients at the same time? With each new incoming connection, NGINX creates a file descriptor, which consumes less memory than an entire thread or process. Because its architecture is event-driven rather than process-based, NGINX also reduces the need for context switching that occurs in process-per-connection web servers.

## HTTP Request

First, we analyze a valid HTTP request from a browser. To gain insights into the HTTP commands, we instruct the local system to begin listening for TCP connections.
```bash
netcat -l 5555
```

Open a browser and access localhost/127.0.0.1 on port 5555
```bash
google-chrome --incognito http://localhost:5555
```

In the command line we see this sample output
```bash
❯ netcat -l 5555
GET / HTTP/1.1
Host: localhost:5555
Connection: keep-alive
sec-ch-ua: "Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"
sec-ch-ua-mobile: ?0
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: none
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Accept-Encoding: gzip, deflate, br
Accept-Language: en
```

The important lines:
- Request-Line: The Request-Line begins with a method token `GET`, followed by the Request-URI `/` and the protocol version `HTTP/1.1`. ([HTTP/1.1: Request](https://www.w3.org/Protocols/rfc2616/rfc2616-sec5.html))
- Host request-header: Required Host header field `localhost:5555`. ([HTTP/1.1: Header Field Definitions](https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.23))
- No CR or LF is allowed except in the final CRLF sequence.

We send `GET / HTTP/1.1` followed by a `Host: localhost` and two `[Enter]` newlines.
```bash
❯ telnet 10.0.0.10 80
Trying 10.0.0.10...
Connected to 10.0.0.10.
Escape character is '^]'.
GET / HTTP/1.1
Host: localhost


```

## Slow HTTP Test
```bash
slowhttptest -c 500 -H -g -o ./output_file -i 10 -r 200 -t GET -u http://10.0.0.10 -x 24 -p 2
```

## Links
- [What is a low and slow attack? Low and slow DDoS attack definition | Cloudflare](https://www.cloudflare.com/learning/ddos/ddos-low-and-slow-attack/)
- [An overview of HTTP - HTTP | MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview)
- [Monitoring Apache Web Server Performance | Datadog](https://www.datadoghq.com/blog/monitoring-apache-web-server-performance/)
- [How to perform a DoS attack "Slow HTTP" with SlowHTTPTest (test your server Slowloris protection) in Kali Linux | Our Code World](https://ourcodeworld.com/articles/read/949/how-to-perform-a-dos-attack-slow-http-with-slowhttptest-test-your-server-slowloris-protection-in-kali-linux)
